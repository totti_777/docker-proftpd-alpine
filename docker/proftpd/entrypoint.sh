#!/bin/bash

if [ ! -f /etc/timezone ] && [ ! -z "$TZ" ]; then
  # At first startup, set timezone
  cp /usr/share/zoneinfo/$TZ /etc/localtime
  echo $TZ >/etc/timezone
fi

if [ -z "$PASV_ADDRESS" ]; then
  echo "** This container will not run without setting for PASV_ADDRESS **"
  sleep 10
  exit 1
fi


if [ -n "$FTP_LIST" ]; then
        IFS=';' read -r -a parsed_ftp_list <<< "$FTP_LIST" ; unset IFS
        for ftp_account in ${parsed_ftp_list[@]}
        do
                IFS=':' read -r -a tab <<< "$ftp_account" ; unset IFS
                ftp_login=${tab[0]}
                ftp_pass=${tab[1]}
                ftp_uid=${tab[2]}
                mkdir /home/${ftp_login}
		cd /etc/proftpd
                echo ${ftp_pass} | ftpasswd --stdin --passwd --sha512 --name=${ftp_login} --uid=${ftp_uid} --home=/home/${ftp_login} --shell=/bin/false
		chown ${ftp_uid}:${ftp_uid} /home/${ftp_login}
        done
fi

if [ "$TLS_ENABLED" == 'on' ]; then
	apk add --no-cache openssl proftpd-mod_tls
	mkdir /etc/proftpd/ssl && chown proftpd /etc/proftpd
	openssl req -new -x509 -days 365 -nodes -out /etc/proftpd/ssl/proftpd.crt -keyout /etc/proftpd/ssl/proftpd.key -subj '/CN=proftpd'
fi

mkdir -p /run/proftpd && chown proftpd /run/proftpd/

sed -i \
    -e "s:{{ ALLOW_OVERWRITE }}:$ALLOW_OVERWRITE:" \
    -e "s:{{ ANONYMOUS_DISABLE }}:$ANONYMOUS_DISABLE:" \
    -e "s:{{ ANON_UPLOAD_ENABLE }}:$ANON_UPLOAD_ENABLE:" \
    -e "s:{{ LOCAL_UMASK }}:$LOCAL_UMASK:" \
    -e "s:{{ MAX_CLIENTS }}:$MAX_CLIENTS:" \
    -e "s:{{ MAX_INSTANCES }}:$MAX_INSTANCES:" \
    -e "s:{{ PASV_ADDRESS }}:$PASV_ADDRESS:" \
    -e "s:{{ PASV_MAX_PORT }}:$PASV_MAX_PORT:" \
    -e "s:{{ PASV_MIN_PORT }}:$PASV_MIN_PORT:" \
    -e "s+{{ SERVER_NAME }}+$SERVER_NAME+" \
    -e "s:{{ TIMES_GMT }}:$TIMES_GMT:" \
    -e "s:{{ WRITE_ENABLE }}:$WRITE_ENABLE:" \
    -e "s:{{ TLS_ENABLED }}:$TLS_ENABLED:" \
    /etc/proftpd/proftpd.conf

exec proftpd --nodaemon -c /etc/proftpd/proftpd.conf
